Laravel 5.5
PHP 7.0
MySql 5.7
Runs with Docker locally (or with VBbox, Vagrant, Homestead)

Authentication required to access content.

Home page pulls top five movies from the database and sorts by release year.  JSON format available through the link at the bottom of the page.

Additional "Star Wars" page uses Guzzle to pull from a free star wars api and sorts films by episode and formats dates.

Bootstrap implemented.

If you want to run with docker-compose
*make sure you have downloaded docker https://www.docker.com/get-started

*cd into project folder

*run: docker-compose up

*run: docker-compose exec app php artisan migrate

*run: docker-compose exec app php artisan db:seed 

*go to http://localhost:8080