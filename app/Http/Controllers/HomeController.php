<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()) {
            $movies = Movie::orderBy('year', 'desc')->get();
            $currentYear = date("Y");
            return view('home', compact('movies','currentYear'));
        }
        else
            return redirect()->route('login');
    }

    public function json(){
        if(Auth::check()) {
            $movies = Movie::orderBy('year', 'desc')->get();
            return response()->json($movies);
        }
        else
            return redirect()->route('login');
    }
}
