<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class StarWarsController extends Controller
{

    public function index()
    {
        if(Auth::check()) {
            $client = new Client();
            $response = $client->get('http://swapi.co/api/films');

            $result = json_decode($response->getBody()->getContents())->results;
            $sorted = collect($result)->sortBy('episode_id');

            $sorted->values()->all();

            return view('starWars', compact('sorted'));
        }
        else
            return redirect()->route('login');
    }

}
