@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="content">
                    <div class="title m-b-md">
                        Top 5 Movies
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach ($movies as $movie)
                        <div class="card">
                            <div class="card-body">
                                <img src="/images/{{$movie->image}}" width="50">
                                @if($movie->title == 'Star Wars Series')
                                    <a href="/starWars">{{$movie->title}}</a>
                                @else
                                    {{$movie->title}}
                                @endif
                                {{$movie->year}} - (Released {{ ((int)$currentYear - (int)$movie['year'])}} years ago)<br />
                            </div>
                        </div>
                    @endforeach
                    <center><a href="/json">Get the JSON</a></center>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
