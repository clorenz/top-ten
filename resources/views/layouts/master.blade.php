<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Carrie's Coding Exercise</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="/css/appTwo.css" />
</head>
<body>

    <nav class="navbar fixed-top navbar-offset navbar-expand-lg navbar-light bg-light">
        <img class="circular--square" style="float: left; margin-left: 10px; margin-right: 10px;" src="/images/profile_pic_square.jpg" width="75">
        <a class="navbar-brand" href="/">Carrie's Coding Exercise
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
            <ul class="navbar-nav">
                @guest
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="/home">Top 5</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/starWars">Star Wars</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endguest
            </ul>
            @auth
                <span class="navbar-text">
                    {{ Auth::user()->name }}
                </span>
            @endauth
         </div>
    </nav>

    <div class="container" class="container-fluid scrollable" style="padding-top:70px;padding-bottom:70px;">
        <div id="content">
        <div>
            @yield('content')
        </div>
        </div>
    </div>
    <div class="navbar navbar-fixed-bottom site-footer" style="margin-bottom: -1px;">
        <p class="footer-info">&copy;2019 Carrie Lorenz</p>
    </div>
</body>
</html>
