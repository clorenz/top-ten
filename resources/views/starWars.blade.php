
@extends('layouts.master')
@section('content')


<div class="content">
    <div class="jumbotron">
      <h1 class="display-4">Star Wars, Baby!</h1>
      <p class="lead">A list of Star Wars Movies.</p>
      <hr class="my-4">
      <p>Data provided by <a href="https://swapi.co" target="new">https://swapi.co</a></p>
    </div>
    @foreach ($sorted as $film)
    <div class="card">
        <div class="card-body">
        <h3><strong>{{$film->title}}</strong></h3>
        <h6 class="card-title"><strong>Director:</strong> {{$film->director}}</h5>
        <h6 class="card-title"><strong>Episode:</strong> {{$film->episode_id}}</h5>
        <h6 class="card-title"><strong>Release Date:</strong> {{date('m/d/Y', strtotime($film->release_date))}}</h5>
        <div>{{$film->opening_crawl}}</div>
        </div>
    </div>
    @endforeach

</div>


