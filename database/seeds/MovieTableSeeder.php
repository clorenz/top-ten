<?php

use Illuminate\Database\Seeder;

class MovieTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('movie')->insert([
            'title' => 'The Big Lebowski',
            'year' => 1998,
            'image' => 'lebowski.jpg'
        ]);
        DB::table('movie')->insert([
            'title' => 'Star Wars Series',
            'year' => 1977,
            'image' => 'starwars.jpeg'
        ]);
        DB::table('movie')->insert([
            'title' => 'Napolean Dynamite',
            'year' => 2004,
            'image' => 'napoleon.jpeg'
        ]);
        DB::table('movie')->insert([
            'title' => 'Lord of the Rings Series',
            'year' => 2001,
            'image' => 'lotr.jpeg'
        ]);
        DB::table('movie')->insert([
            'title' => 'Mad Max: Fury Road',
            'year' => 2015,
            'image' => 'max.jpeg'
        ]);
    }

}
